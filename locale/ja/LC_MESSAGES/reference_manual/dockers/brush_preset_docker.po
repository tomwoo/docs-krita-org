msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:1
msgid "Overview of the brush presets docker."
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Brush Preset"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Brush"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Presets"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Paintop Presets"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:16
msgid "Preset Docker"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:19
msgid ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:20
msgid ""
"This docker allows you to switch the current brush you're using, as well as "
"tagging the brushes."
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:22
msgid "Just |mouseleft| on an icon to switch to that brush!"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:25
msgid "Tagging"
msgstr ""

#: ../../reference_manual/dockers/brush_preset_docker.rst:27
msgid "|mouseright| a brush to add a tag or remove a tag."
msgstr ""
