# Translation of docs_krita_org_reference_manual___tools___zoom.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___zoom\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:37+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"

#: ../../<rst_epilog>:82
msgid ""
".. image:: images/icons/zoom_tool.svg\n"
"   :alt: toolzoom"
msgstr ""
".. image:: images/icons/zoom_tool.svg\n"
"   :alt: toolzoom"

#: ../../reference_manual/tools/zoom.rst:1
msgid "Krita's zoom tool reference."
msgstr "Довідник із інструмента масштабування Krita."

#: ../../reference_manual/tools/zoom.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/zoom.rst:11
msgid "Zoom"
msgstr "Масштаб"

#: ../../reference_manual/tools/zoom.rst:16
msgid "Zoom Tool"
msgstr "Масштабування"

#: ../../reference_manual/tools/zoom.rst:18
msgid "|toolzoom|"
msgstr "|toolzoom|"

#: ../../reference_manual/tools/zoom.rst:20
msgid ""
"The zoom tool allows you to zoom your canvas in and out discretely. It can "
"be found at the bottom of the toolbox, and you just activate it by selecting "
"the tool, and doing |mouseleft| on the canvas will zoom in, while |"
"mouseright| will zoom out."
msgstr ""
"За допомогою інструмента масштабування можна дискретно збільшувати і "
"зменшувати масштаб на вашому полотні. Кнопку інструмента розташовано у "
"нижній частині панелі інструментів. Ви можете активувати її натисканням "
"кнопки інструмента. Далі, можна скористатися клацанням |mouseleft| на "
"полотні для збільшення масштабу. Клацанням |mouseright| можна зменшити "
"масштаб."

#: ../../reference_manual/tools/zoom.rst:22
msgid "You can reverse this behavior in the :guilabel:`Tool Options`."
msgstr ""
"Обернути цю поведінку можна за допомогою бічної панелі :guilabel:`Параметри "
"інструмента`."

#: ../../reference_manual/tools/zoom.rst:24
msgid ""
"There's a number of hotkeys associated with this tool, which makes it easier "
"to access from the other tools:"
msgstr ""
"Із цим інструментом пов'язано декілька клавіатурних скорочень, які спрощують "
"доступ до нього з інших інструментів:"

#: ../../reference_manual/tools/zoom.rst:26
msgid ""
":kbd:`Ctrl + Space +` |mouseleft| :kbd:`+ drag` on the canvas will zoom in "
"or out fluently."
msgstr ""
":kbd:`Ctrl + Пробіл` + |mouseleft| + перетягування на полотні збільшує або "
"зменшує масштаб у неперервний спосіб."

#: ../../reference_manual/tools/zoom.rst:27
msgid ""
":kbd:`Ctrl +` |mousemiddle| :kbd:`+ drag` on the canvas will zoom in or out "
"fluently."
msgstr ""
":kbd:`Ctrl` + |mousemiddle| + перетягування на полотні збільшує або зменшує "
"масштаб у неперервний спосіб."

#: ../../reference_manual/tools/zoom.rst:28
msgid ""
":kbd:`Ctrl + Alt + Space +` |mouseleft| :kbd:`+ drag` on the canvas will "
"zoom in or out with discrete steps."
msgstr ""
":kbd:`Ctrl + Alt + Пробіл` + |mouseleft| + перетягування на полотні збільшує "
"або зменшує масштаб дискретними кроками."

#: ../../reference_manual/tools/zoom.rst:29
msgid ""
":kbd:`Ctrl + Alt +` |mousemiddle| :kbd:`+ drag` on the canvas will zoom in "
"or out with discrete steps."
msgstr ""
":kbd:`Ctrl + Alt +` |mousemiddle| + перетягування на полотні збільшує або "
"зменшує масштаб дискретними кроками."

#: ../../reference_manual/tools/zoom.rst:30
msgid ":kbd:`+` will zoom in with discrete steps."
msgstr ":kbd:`+` збільшує масштаб дискретними кроками."

#: ../../reference_manual/tools/zoom.rst:31
msgid ":kbd:`-` will zoom out with discrete steps."
msgstr ":kbd:`-` зменшує масштаб дискретними кроками."

#: ../../reference_manual/tools/zoom.rst:32
msgid ":kbd:`1` will set the zoom to 100%."
msgstr ":kbd:`1` встановлює масштаб у 100%."

#: ../../reference_manual/tools/zoom.rst:33
msgid ""
":kbd:`2` will set the zoom so that the document fits fully into the canvas "
"area."
msgstr ""
":kbd:`2` змінює масштаб так, щоб документ повністю вміщувався у область "
"полотна."

#: ../../reference_manual/tools/zoom.rst:34
msgid ""
":kbd:`3` will set the zoom so that the document width fits fully into the "
"canvas area."
msgstr ""
":kbd:`3` змінює масштаб так, щоб документ повністю вміщувався за шириною у "
"область полотна."

#: ../../reference_manual/tools/zoom.rst:36
msgid "For more information on such hotkeys, check :ref:`navigation`."
msgstr ""
"Докладніші відомості щодо клавіатурних скорочень можна знайти у розділі :ref:"
"`navigation`."
