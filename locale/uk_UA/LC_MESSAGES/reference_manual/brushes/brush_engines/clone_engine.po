# Translation of docs_krita_org_reference_manual___brushes___brush_engines___clone_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___clone_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:45+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Clone from all visible layers"
msgstr "Клонувати з усіх видимих шарів"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:1
#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:18
msgid "The Clone Brush Engine manual page."
msgstr "Розділ підручника щодо рушія пензлів клонування."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:16
msgid "Clone Brush Engine"
msgstr "Рушій пензлів клонування"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
msgid "Clone Tool"
msgstr "Інструмент клонування"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:30
msgid ".. image:: images/icons/clonebrush.svg"
msgstr ".. image:: images/icons/clonebrush.svg"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:31
msgid ""
"The clone brush is a brush engine that allows you to paint with a "
"duplication of a section of a paint-layer. This is useful in manipulation of "
"photos and textures. You have to select a source and then you can paint to "
"copy or clone the source to a different area. Other applications normally "
"have a separate tool for this, Krita has a brush engine for this."
msgstr ""
"Пензель клонування — рушій пензлів, за допомогою якого ви можете намалювати "
"дублікат якоїсь ділянки на шарі малювання. Цей пензель корисний для обробки "
"фотографій та створення текстур. Ви можете позначити початкову ділянку, а "
"потім намалювати її копію або клон у іншому місці зображення. У інших "
"програмах для цього є окремий інструмент, але у Krita можна просто "
"скористатися відповідним рушієм пензлів."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:34
msgid "Usage and Hotkeys"
msgstr "Користування та клавіатурні скорочення"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:36
msgid ""
"To see the source, you need to set the brush-cursor settings to brush "
"outline."
msgstr ""
"Щоб бачити джерело, вам слід налаштувати у параметрах вказівника-пензля "
"показ ескіза пензля."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:38
msgid ""
"The clone tool can now clone from the projection and it's possible to change "
"the clone source layer. Press the :kbd:`Ctrl + Alt +` |mouseleft| shortcut "
"to select a new clone source on the current layer. The :kbd:`Ctrl +` |"
"mouseleft| shortcut to select a new clone source point on the layer that was "
"active when you selected the clone op."
msgstr ""
"Інструмент клонування може клонувати дані з проєкції, також можна змінити "
"шар початкового зображення для клону. Натисніть комбінацію :kbd:`Ctrl + Alt "
"+` |mouseleft|, щоб вибрати нове джерело для клонування на поточному шарі. "
"Комбінацією :kbd:`Ctrl +` |mouseleft| можна скористатися для вибору нової "
"точки джерела клонування на шарі, який був активним, коли ви вибрали пензель "
"клонування."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:42
msgid ""
"The :kbd:`Ctrl + Alt +` |mouseleft| shortcut is temporarily disabled on "
"2.9.7."
msgstr ":kbd:`Ctrl + Alt +` |mouseleft| тимчасово вимкнено з версії 2.9.7."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:45
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:47
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:48
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:49
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:52
msgid "Painting mode"
msgstr "Режим малювання"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:54
msgid "Healing"
msgstr "Лікування"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:55
msgid ""
"This turns the clone brush into a healing brush: often used for removing "
"blemishes in photo retouching, and maybe blemishes in painting."
msgstr ""
"Перетворює пензель клонування у лікувальний пензель. Часто використовується "
"для вилучення дефектів при ретушуванні фотографії. Може бути використано для "
"усування дефектів при малюванні."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:57
msgid "Only works when there's a perspective grid visible."
msgstr "Працює, лише якщо є видимою ґратка перспективи."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:59
msgid "Perspective correction"
msgstr "Виправлення перспективи"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:60
msgid "This feature is currently disabled."
msgstr "Цю можливість у поточній версії вимкнено."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:61
msgid "Source Point move"
msgstr "Пересування початкової точки"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:62
msgid ""
"This will determine whether you will replicate the source point per dab or "
"per stroke. Can be useful when used with the healing brush."
msgstr ""
"Цей параметр визначає, відтворюватиметься початкова точка мазка чи штриха. "
"Може бути корисним, якщо використовується у поєднанні із лікувальним пензлем."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:63
msgid "Source Point reset before a new stroke"
msgstr "Скидання початкової точки перед новим мазком"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:64
msgid ""
"This will reset the source point everytime you make a new stroke. So if you "
"were cloning a part in one stroke, having this active will allow you to "
"clone the same part again in a single stroke, instead of using the source "
"point as a permanent offset."
msgstr ""
"За допомогою цього пункту можна скидати початкову точку кожного разу, коли "
"ви робите новий мазок. Отже, якщо ви клонуєте якусь частину зображення одним "
"мазком, якщо позначено цей пункт, ви зможете клонувати ту саму частину "
"наступним мазком, не використовуючи сталий відступ від початкової точки."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:66
msgid ""
"Tick this to force cloning of all layers instead of just the active one."
msgstr "Позначте цей пункт, якщо клонувати слід усі шари, а не лише активний."
