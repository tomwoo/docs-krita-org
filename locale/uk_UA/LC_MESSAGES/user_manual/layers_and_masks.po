# Translation of docs_krita_org_user_manual___layers_and_masks.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___layers_and_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 17:01+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../user_manual/layers_and_masks.rst:None
msgid ".. image:: images/layers/Layer-color-filters.png"
msgstr ".. image:: images/layers/Layer-color-filters.png"

#: ../../user_manual/layers_and_masks.rst:None
msgid ".. image:: images/layers/Layer-color-filters-menu.png"
msgstr ".. image:: images/layers/Layer-color-filters-menu.png"

#: ../../user_manual/layers_and_masks.rst:None
msgid ".. image:: images/layers/500px-Krita-types-of-layers.png"
msgstr ".. image:: images/layers/500px-Krita-types-of-layers.png"

#: ../../user_manual/layers_and_masks.rst:1
msgid "An introduction guide to how layers and masks work inside Krita."
msgstr "Вступні настанови щодо роботи шарів і масок у Krita."

#: ../../user_manual/layers_and_masks.rst:15
msgid "Layers"
msgstr "Шари"

#: ../../user_manual/layers_and_masks.rst:15
msgid "Masks"
msgstr "Маски"

#: ../../user_manual/layers_and_masks.rst:20
msgid "Introduction to Layers and Masks"
msgstr "Вступ до шарів і масок"

#: ../../user_manual/layers_and_masks.rst:22
msgid ""
"Krita supports layers which help to better control parts and elements of "
"your painting."
msgstr ""
"У Krita передбачено шари, які допоможуть вам краще керувати частинами та "
"елементами вашого малюнка."

#: ../../user_manual/layers_and_masks.rst:25
msgid ""
"Think of an artwork or collage made with various stacks of papers with some "
"paper cut such that they show the paper beneath them while some hide what's "
"beneath them. If you want to replace an element in the artwork, you replace "
"that piece of paper instead of drawing the entire thing. In Krita instead of "
"papers we use **Layers**. Layers are part of the document which may or may "
"not be transparent, they may be smaller or bigger than the document itself, "
"they can arrange one above other, named and grouped."
msgstr ""
"Розгляньмо художній твір як колаж, створений із стосу малюнків на папері із "
"вирізами, створеними таким чином, щоб показувати зображення під ними або "
"частково приховувати його. Якщо вам потрібно замінити частину твору, ви не "
"перемальовуєте увесь твір, а замінюєте один із папірців. У Krita замість "
"папірців ви маєте справу із **шарами**. Шари є частиною документа. Вони "
"можуть бути прозорими або непрозорими. Вони можуть бути меншими або більшими "
"за сам документ. Їх можна розташовувати один над одним, визначати для них "
"назви та групувати їх."

#: ../../user_manual/layers_and_masks.rst:34
msgid ""
"Layers can give better control over your artwork for example you can re-"
"color an entire artwork just by working on the separate color layer and "
"thereby not destroying the line art which will reside above this color layer."
msgstr ""
"За допомогою шарів легше керувати вашим малюнком. Наприклад, ви можете "
"змінити колір усього твору простою зміною кольорі у спеціальному шарі "
"кольорів, нічого не змінюючи у графічній частині роботи, яку буде "
"розташовано у шарі над шаром кольорів."

#: ../../user_manual/layers_and_masks.rst:39
msgid ""
"You can edit individual layers, you can even add special effects to them, "
"like Layer styles, blending modes, transparency, filters and transforms. "
"Krita takes all these layers in its layer stack, including the special "
"effects and combines or composites together a final image. This is just one "
"of the many digital image manipulation tricks that :program:`Krita` has up "
"its sleeve!"
msgstr ""
"Ви можете редагувати окремі шари, ви навіть можете додавати до них "
"спеціальні ефекти, зокрема стилі шарів, режими змішування, прозорість, "
"фільтри та перетворення. Krita збирає усі ці шари, включно із спеціальними "
"ефектами, до свого стосу шарів і поєднує їх або створює композицію шарів, "
"яка утворює остаточне зображення. Це лише один із багатьох інструментів "
"роботи із цифровими зображеннями, який є у арсеналі :program:`Krita`!"

#: ../../user_manual/layers_and_masks.rst:46
msgid ""
"Usually, when you put one paint layer on top of another, the upper paint "
"layer will be fully visible, while the layer behind it will either be "
"obscured, occluded or only partially visible."
msgstr ""
"Зазвичай, коли ви кладете один шар фарби на інший, верхній шар лишається "
"повністю видимим, а шар під ним стає невидимим, частково змішаним із верхнім "
"або видимим лише частково."

#: ../../user_manual/layers_and_masks.rst:51
msgid "Managing layers"
msgstr "Керування шарами"

#: ../../user_manual/layers_and_masks.rst:53
msgid ""
"Some artists draw with limited number of layers but some prefer to have "
"different elements of the artwork on separate layer. Krita has some good "
"layer management features which make the layer management task easy."
msgstr ""
"Деякі художники надають перевагу малюванню із обмеженою кількістю шарів, а "
"деякі розташовують кожен з елементів зображення на окремому шарі. У Krita "
"передбачено деякі чудові можливості з керування шарами, які роблять це "
"завдання дуже простим."

#: ../../user_manual/layers_and_masks.rst:57
msgid ""
"You can :ref:`group layers <group_layers>` and organise the elements of your "
"artwork."
msgstr ""
"Ви можете :ref:`згрупувати шари <group_layers>` і упорядкувати елементи "
"вашої художньої роботи."

#: ../../user_manual/layers_and_masks.rst:60
msgid ""
"The layer order can be changed or layers can be moved in and out of a group "
"in the layer stack by simply holding them and dragging and dropping. Layers "
"can also be copied across documents while in the :ref:`subwindow mode "
"<window_settings>`, by dragging and dropping from one document to another."
msgstr ""
"Ви можете змінити порядок шарів, пересунути шар до групи або виокремити його "
"з групи у стосі шарів простим перетягуванням пунктів шарів у списку зі "
"скиданням їх на нове місце. Шари можна копіювати з одного документа до "
"іншого у :ref:`режимі підвікон <window_settings>` перетягуванням шару з "
"одного документа із наступним скиданням його до іншого."

#: ../../user_manual/layers_and_masks.rst:66
msgid ""
"These features save time and also help artists in maintaining the file with "
"a layer stack which will be easy to understand for others who work on the "
"same file. In addition to these layers and groups can both be labeled and "
"filtered by colors, thus helping the artists to visually differentiate them."
msgstr ""
"Ці можливості заощаджують час та допомагають художникам у підтриманні такого "
"порядку у стосі шарів, який просто зрозуміти іншим художникам, які "
"працюватимуть над тим самим файлом. Окрім цього, і шари і групи шарів можна "
"позначати мітками і фільтрувати за кольором. Це спрощує візуальну "
"диференціацію шарів для художників."

#: ../../user_manual/layers_and_masks.rst:72
msgid ""
"To assign a color label to your layer or layer group you have to right click "
"on the layer and choose one of the given colors from the context menu. To "
"remove an already existing color label you can click on the 'x' marked box "
"in the context menu."
msgstr ""
"Щоб призначити кольорову мітку до вашого шару або групи шарів, достатньо "
"клацнути правою кнопкою миші на пункті у списку і вибрати один із заданих "
"кольорів у контекстному меню. Щоб вилучити наявну кольорову мітку, натисніть "
"у контекстному меню пункт, який позначено символом «x»."

#: ../../user_manual/layers_and_masks.rst:80
msgid ""
"Once you assign color labels to your layers, you can then filter layers "
"having similar color label by clicking on one or more colors in the list "
"from the drop-down situated at the top-right corner of the layer docker."
msgstr ""
"Після пов'язування шарів із кольоровими мітками ви можете отримувати список "
"шарів, які мають подібні кольорові мітки, простим натисканням одного або "
"декількох кольорів у списку, який розкривається натисканням кнопки у "
"верхньому правому куті бічної панелі шарів."

#: ../../user_manual/layers_and_masks.rst:88
msgid "Types of Layers"
msgstr "Типи шарів"

#: ../../user_manual/layers_and_masks.rst:93
msgid ""
"The image above shows the various types of layers in :ref:`layer_docker`. "
"Each layer type has a different purpose for example all the vector elements "
"can be only placed on a vector layer and similarly normal raster elements "
"are mostly on the paint layer, :ref:`cat_layers_and_masks` page contains "
"more information about these types layers."
msgstr ""
"На наведеному вище зображенні показані різні типи шарів на панелі :ref:"
"`layer_docker`. У кожного типу шарів власне призначення. Наприклад, усі "
"векторні елементи можна розташовувати лише у векторних шарах, а звичайні "
"растрові елементи, здебільшого, слід розташовувати у шарі малювання. "
"Докладніші відомості щодо цих типів шарів можна знайти на сторінці :ref:"
"`cat_layers_and_masks`."

#: ../../user_manual/layers_and_masks.rst:95
msgid "Now Let us see how these layers are composited in Krita."
msgstr "Тепер давайте поглянемо, які ці шари поєднуються у Krita."

#: ../../user_manual/layers_and_masks.rst:98
msgid "How are layers composited in Krita ?"
msgstr "Як поєднуються шари у Krita?"

#: ../../user_manual/layers_and_masks.rst:100
msgid ""
"In Krita, the visible layers form a composite image which is shown on the "
"canvas. The order in which Krita composites the layers is from bottom to "
"top, much like the stack of papers we discussed above. As we continue adding "
"layers, the image we see changes, according to the properties of the newly "
"added layers on top. Group Layers composite separately from the other layers "
"in the stack, except when pass through mode is activated. The layers inside "
"a group form a composite image first and then this composite is taken into "
"consideration while the layer stack is composited to form a whole image. If "
"the pass through mode is activated by pressing the icon similar to bricked "
"wall, the layers within the group are considered as if they are outside of "
"that particular group in the layer stack, however, the visibility of the "
"layers in a group depends on the visibility of the group."
msgstr ""
"У Krita видимі шари формують композитне зображення, яке програма показує на "
"полотні. Krita поєднує шари знизу вгору, подібно до стосу паперів, який ми "
"обговорювали раніше. Із додаванням шарів зображення змінюється відповідно до "
"властивостей доданих згори шарів. Якщо не увімкнено режим передавання, "
"композиція групи шарів створюється окремо від інших шарів у стосі. Спочатку "
"програма формує композитне зображення групи шарів, а потім, під час "
"створення усього зображення зі стосу шарів, враховує цю композицію. Якщо "
"увімкнено режим передавання натисканням піктограми, подібної до цегляної "
"стіни, шари у групі враховуються так, наче вони перебувають поза певною "
"групою у стосі шарів. Втім, видимість шарів у групі у цьому режимі залежить "
"від видимості групи."

#: ../../user_manual/layers_and_masks.rst:115
msgid ".. image:: images/layers/Passthrough-mode_.png"
msgstr ".. image:: images/layers/Passthrough-mode_.png"

#: ../../user_manual/layers_and_masks.rst:117
msgid ".. image:: images/layers/Layer-composite.png"
msgstr ".. image:: images/layers/Layer-composite.png"

#: ../../user_manual/layers_and_masks.rst:118
msgid ""
"The groups in a PSD file saved from Photoshop have pass-through mode on by "
"default unless they are specifically set with other blending modes."
msgstr ""
"Для груп у файлі PSD, який збережено у Photoshop, передбачено типовий режим "
"передавання, якщо спеціально не встановлено інший режим змішування "
"зображення."

#: ../../user_manual/layers_and_masks.rst:121
msgid "Alpha Inheritance"
msgstr "Успадкування прозорості"

#: ../../user_manual/layers_and_masks.rst:121
msgid "Clipping Masks"
msgstr "Маски обрізання"

#: ../../user_manual/layers_and_masks.rst:124
msgid "Inherit Alpha or Clipping layers"
msgstr "Успадкування прозорості або обрізання шарів"

#: ../../user_manual/layers_and_masks.rst:126
msgid ""
"There is a clipping feature in Krita called inherit alpha. It is denoted by "
"an alpha icon in the layer stack."
msgstr ""
"У Krita можливість обрізання називається успадкуванням прозорості. На "
"відповідній кнопці у стоці шарів ви можете бачити піктограму із грецькою "
"літерою альфа."

#: ../../user_manual/layers_and_masks.rst:130
msgid ".. image:: images/layers/Inherit-alpha-02.png"
msgstr ".. image:: images/layers/Inherit-alpha-02.png"

#: ../../user_manual/layers_and_masks.rst:131
msgid ""
"It can be somewhat hard to figure out how the inherit alpha feature works in "
"Krita for the first time. Once you click on the inherit alpha icon on the "
"layer stack, the pixels of the layer you are painting on are confined to the "
"combined pixel area of all the layers below it. That means if you have the "
"default white background layer as first layer, clicking on the inherit alpha "
"icon and painting on any layer above will seem to have no effect as the "
"entire canvas is filled with white. Hence, it is advised to put the base "
"layer that you want the pixels to clip in a group layer. As mentioned above, "
"group layers are composited separately, hence the layer which is the lowest "
"layer in a group becomes the bounding layer and the content of the layers "
"above this layer clips to it if inherit alpha is enabled."
msgstr ""
"На початку, може бути дещо складно розібратися із тим, як працює можливість "
"успадкування шарів у Krita. Щойно ви натиснете піктограму успадкування "
"прозорості у стосі шарів, пікселі шару, на якому ви малюєте, буде обмежено "
"областю усіх пікселів шарів під ним. Це означає, що якщо першим шаром є "
"типовий шар білого тла, після натискання піктограми успадкування прозорості "
"і малювання на будь-якому шарі над цим шаром не призводитиме ні до яких "
"видимих результатів — усе полотно лишатиметься білим. Отже, радимо додавати "
"базовий шар, з яким мають поєднуватися пікселі, до групи шарів. Як ми вже "
"згадували вище, композиція шарів у групі обчислюється окремо, отже шар, який "
"є найнижчим у групі є обмежувальним шаром, а вміст усіх шарів над ним "
"накладається на цей шар, якщо увімкнено успадкування прозорості."

#: ../../user_manual/layers_and_masks.rst:145
msgid ".. image:: images/layers/Inherit-alpha-krita.jpg"
msgstr ".. image:: images/layers/Inherit-alpha-krita.jpg"

#: ../../user_manual/layers_and_masks.rst:147
msgid ".. image:: images/layers/Krita-tutorial2-I.1-2.png"
msgstr ".. image:: images/layers/Krita-tutorial2-I.1-2.png"

#: ../../user_manual/layers_and_masks.rst:148
msgid "You can also enable alpha inheritance to a group layer."
msgstr "Крім того, ви можете увімкнути успадкування прозорості для шару групи."

#: ../../user_manual/layers_and_masks.rst:151
msgid "Masks and Filters"
msgstr "Маски і фільтри"

#: ../../user_manual/layers_and_masks.rst:153
msgid ""
"Krita supports non-destructive editing of the content of the layer. Non-"
"destructive editing means editing or changing a layer or image without "
"actually changing the original source image permanently, the changes are "
"just added as filters or masks over the original image while keeping it "
"intact, this helps a lot when your workflow requires constant back and "
"forth. You can go back to original image with a click of a button. Just hide "
"the filter or mask you have your initial image."
msgstr ""
"У Krita передбачено підтримку неруйнівного редагування вмісту шарів. "
"Неруйнівним ми називаємо таке редагування, під час якого редагування або "
"внесення змін до шару або зображення не призводить до внесення остаточних "
"змін до початкового зображення. Зміни просто додаються як фільтри або маски "
"над початковим зображенням, лишаючи його незмінним. Такий режим редагування "
"є дуже корисним, якщо робота над зображенням передбачає певні експерименти "
"із додаванням і вилученням ефектів. Ви можете повернутися до початкового "
"зображення одним клацанням кнопкою миші. Просто приховайте фільтр або маску, "
"які ви наклали на початкове зображення."

#: ../../user_manual/layers_and_masks.rst:161
msgid ""
"You can add various filters to a layer with Filter mask, or add Filter layer "
"which will affect the whole image. Layers can also be transformed non-"
"destructively with the transformation masks, and even have portions "
"temporarily hidden with a Transparent Mask. Non-destructive effects like "
"these are very useful when you change your mind later, or need to make a set "
"of variations of a given image."
msgstr ""
"Ви можете додавати для шару фільтри за допомогою маски фільтрування або "
"додавати шар фільтрування, який змінюватиме усе зображення. Шари також можна "
"перетворювати у неруйнівний спосіб за допомогою масок перетворення, і навіть "
"частково приховувати за допомогою маски прозорості. Неруйнівні ефекти, "
"подібні до цих, є дуже корисними, якщо пізніше ви зміните задум або захочете "
"внести певні корективи до зображення."

#: ../../user_manual/layers_and_masks.rst:165
msgid ""
"You can merge all visible layers by selecting everything first :"
"menuselection:`Layer --> Select --> Visible Layers`. Then Combine them all "
"by merging :menuselection:`Layer --> Merge with Layer Below`."
msgstr ""
"Ви можете об'єднати усі видимі шари. Спочатку позначте усі шари у списку за "
"допомогою пункту меню :menuselection:`Шар --> Вибір --> Видимі шари`. Потім "
"об'єднайте усі шари: :menuselection:`Шар --> Об'єднати з шаром нижче`."

#: ../../user_manual/layers_and_masks.rst:167
msgid ""
"These filters and masks are accessible through the right click menu (as "
"shown in the image below) and the Plus icon on the layer docker."
msgstr ""
"Доступ до цих фільтрів і масок можна здійснювати за допомогою контекстного "
"меню (як це показано на знімку, наведеному нижче) та піктограми :guilabel:`"
"+` на бічній панелі шарів."

#: ../../user_manual/layers_and_masks.rst:171
msgid ".. image:: images/layers/Layer-right-click.png"
msgstr ".. image:: images/layers/Layer-right-click.png"

#: ../../user_manual/layers_and_masks.rst:172
msgid ""
"You can also add a filter as a mask from filter dialog itself, by clicking "
"on the :guilabel:`Create Filter Mask` button."
msgstr ""
"Крім того, ви можете додати фільтр як маску із самого діалогового вікна "
"фільтрів. Достатньо просто натиснути кнопку :guilabel:`Створити маску "
"фільтрування`."

#: ../../user_manual/layers_and_masks.rst:176
msgid ".. image:: images/layers/Filtermask-button.png"
msgstr ".. image:: images/layers/Filtermask-button.png"

#: ../../user_manual/layers_and_masks.rst:177
msgid ""
"All the filters and masks can also be applied over a group too, thus making "
"it easy to non-destructively edit multiple layers at once. In the :ref:"
"`category Layers and masks <cat_layers_and_masks>` you can read more about "
"the individual types of layers and masks."
msgstr ""
"Усі фільтри і маски можна застосовувати і до групи шарів, що робить дуже "
"простим неруйнівне редагування одразу декількох шарів. На сторінці :ref:"
"`категорії «Шари і маски» <cat_layers_and_masks>` наведено докладніші "
"відомості щодо окремих типів шарів та масок."

#: ../../user_manual/layers_and_masks.rst:182
msgid ""
":ref:`Layer Docker <layer_docker>` has more information about the shortcuts "
"and other layer management workflows."
msgstr ""
"У розділі щодо :ref:`бічної панелі шарів <layer_docker>` наведено докладніші "
"відомості щодо клавіатурних скорочень та інших процедур керування шарами."
