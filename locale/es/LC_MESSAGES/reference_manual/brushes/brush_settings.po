# Spanish translations for docs_krita_org_reference_manual___brushes___brush_settings.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-20 14:47+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/brushes/brush_settings.rst:5
msgid "Brush Settings"
msgstr "Preferencias del pincel"

#: ../../reference_manual/brushes/brush_settings.rst:7
msgid "Overall Brush Settings for the various brush engines."
msgstr ""
"Resumen de las preferencias del pincel para varios motores de pinceles."

#: ../../reference_manual/brushes/brush_settings.rst:9
msgid "Contents:"
msgstr "Contenido:"
