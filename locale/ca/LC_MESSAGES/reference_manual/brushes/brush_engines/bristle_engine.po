# Translation of docs_krita_org_reference_manual___brushes___brush_engines___bristle_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 19:49+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Weighted saturation"
msgstr "Saturació estilada"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:0
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.3-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.3-1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:0
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.3-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.3-2.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:1
msgid "The Bristle Brush Engine manual page."
msgstr "La pàgina del manual per al motor del pinzell de pèl."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:18
msgid "Bristle Brush Engine"
msgstr "Motor del pinzell de pèl"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Brush Engine"
msgstr "Motor de pinzell"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Hairy Brush Engine"
msgstr "Motor del pinzell de pèl"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Sumi-e"
msgstr "Sumi-e"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:21
msgid ".. image:: images/icons/bristlebrush.svg"
msgstr ".. image:: images/icons/bristlebrush.svg"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:22
msgid ""
"A brush intended to mimic real-life brushes by drawing the trails of their "
"lines or bristles."
msgstr ""
"Un pinzell que pretén imitar els pinzells a la vida real, dibuixant els "
"traços de les seves línies o pèls."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:25
msgid "Brush Tip"
msgstr "Punta del pinzell"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:27
msgid "Simply put:"
msgstr "En poques paraules:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:29
msgid "The brush tip defines the areas with bristles in them."
msgstr "La punta del pinzell defineix les àrees amb pèls."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:30
msgid ""
"Lower opacity areas have lower-opacity bristles. With this brush, this may "
"give the illusion that lower-opacity areas have fewer bristles."
msgstr ""
"Les àrees amb una opacitat baixa tenen pèls d'opacitat baixa. Amb aquest "
"pinzell, es donarà la il·lusió que les àrees amb opacitat baixa tenen menys "
"pèl."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:31
msgid ""
"The :ref:`option_size` and :ref:`option_rotation` dynamics affect the brush "
"tip, not the bristles."
msgstr ""
"La :ref:`option_size` i el :ref:`option_rotation` dinàmics afectaran a la "
"punta del pinzell, no els pèls."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:33
msgid "You can:"
msgstr "Podeu:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:35
msgid ""
"Use different shapes for different effects. Be aware that complex brush "
"shapes will draw more slowly though, while the effects aren't always visible "
"(since in the end, you're passing over an area with a certain number of "
"bristles)."
msgstr ""
"Utilitzar formes diferents per a efectes diferents. Recordeu que les formes "
"de pinzell complexes seran més lentes en dibuixar, tot i que els efectes no "
"sempre seran visibles (ja que haureu de passar sobre una àrea amb un nombre "
"determinat de pèls)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:36
msgid ""
"To decrease bristle density, you can also just use an autobrush and decrease "
"the brush tip's density, or increase its randomness."
msgstr ""
"Per a fer disminuir la densitat del pèl, també podreu utilitzar un pinzell "
"automàtic i fer disminuir la densitat de la punta del pinzell, o bé fer "
"augmentar la seva aleatorietat."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:39
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:41
msgid "Bristle Options"
msgstr "Opcions del pèl"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:43
msgid "The core of this particular brush-engine."
msgstr "Les bases d'aquest motor de pinzell en particular."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:46
msgid ""
"Think of it as pressing down on a brush to make the bristles further apart."
msgstr ""
"Penseu en això com en prémer fort el pinzell perquè els pèls se separin més."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:48
msgid ""
"Larger values basically give you larger brushes and larger bristle spacing. "
"For example, a value of 4 will multiply your base brush size by 4, but the "
"bristles will be 4 times more spaced apart."
msgstr ""
"Els valors més alts crearan pinzells més grans i un espaiat més gran entre "
"els pèls. Per exemple, un valor igual que 4 multiplicarà la mida del pinzell "
"base per 4, però el pèl es quedarà amb 4 vegades més espaiat."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:49
msgid ""
"Use smaller values if you want a \"dense\" brush, i.e. you don't want to see "
"so many bristles within the center."
msgstr ""
"Utilitzeu valors més petits si voleu un pinzell «dens», és a dir, si no "
"voleu veure tants pèls prop del centre."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:50
msgid "Scale"
msgstr "Escala"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:50
msgid ""
"Negative values have the same effect as corresponding positive values: -1.00 "
"will look like 1.00, etc."
msgstr ""
"Els valors negatius tenen el mateix efecte que els respectius valors "
"positius: -1,00 tindrà el mateix aspecte que 1,00, etc."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:53
msgid "Adds a jaggy look to the trailing lines."
msgstr "Afegeix un aspecte dentat a les línies de fuga."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:55
msgid "At 0.00, all the bristles basically remain completely parallel."
msgstr "A 0,00, tots els pèls bàsicament quedaran totalment en paral·lel."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:56
msgid "Random Offset"
msgstr "Desplaçament aleatori"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:56
msgid ""
"At other values, the bristles are offset randomly. Large values will "
"increase the brush size a bit because of the bristles spreading around, but "
"not by much."
msgstr ""
"Amb altres valors, els pèls estaran desplaçats de forma aleatòria. Els "
"valors més alts faran augmentar una mica la mida del pinzell, pel fet que "
"els pèls s'escampen una mica, però no gaire."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:57
msgid "Negative values have the same effect as corresponding positive values."
msgstr ""
"Els valors negatius tenen el mateix efecte que els respectius valors "
"positius."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:58
msgid "Shear"
msgstr "Inclina"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:59
msgid ""
"Shear introduces an angle to your brush, as though you're drawing with an "
"oval brush (or the side of a round brush)."
msgstr ""
"La inclinació introdueix un angle al pinzell, com si estiguéssiu dibuixant "
"amb un pinzell oval (o amb el costat d'un pinzell rodó)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:61
msgid "Density"
msgstr "Densitat"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:61
msgid ""
"This controls the density of bristles. Scale takes a number of bristles and "
"expands or compresses them into a denser area, whereas density takes a fixed "
"area and determines the number of bristles in it. See the difference?"
msgstr ""
"Això controla la densitat dels pèls. L'escala rep un nombre de pèls i els "
"escampa o recull en una àrea més densa, mentre que la densitat tindrà en "
"compte una àrea fixa i definirà el nombre de pèls. Veieu la diferència?"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:64
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.2-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.2-1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:66
msgid ""
"This one maps \"Scale\" to mouse speed, thus simulating pressure with a "
"graphics tablet!"
msgstr ""
"Això associa l'«Escala» a la velocitat del ratolí, simulant així la pressió "
"amb una tauleta de dibuix!"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:68
msgid "Mouse Pressure"
msgstr "Pressió del ratolí"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:68
msgid ""
"Rather, it uses the \"distance between two events\" to determine scale. "
"Faster drawing, larger distances."
msgstr ""
"En el seu lloc, utilitza la «distància entre dos esdeveniments» per a "
"determinar l'escala. Com més ràpid es dibuixi, majors seran les distàncies."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:69
msgid ""
"This doesn't influence the \"pressure\" input for anything else (size, "
"opacity, rotation etc.) so you still have to map those independently to "
"something else."
msgstr ""
"Això no influeix en l'entrada de la «pressió» per a res (mida, opacitat, "
"gir, etc.), de manera que encara els haureu d'associar de forma independent "
"a qualsevulla altra cosa."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:70
msgid "Threshold"
msgstr "Llindar"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:71
msgid ""
"This is a tablet feature. When you turn this on, only bristles that are able "
"to \"touch the canvas\" will be painted."
msgstr ""
"Aquesta és una característica de la tauleta. En activar-la, només es "
"pintaran els pèls que aconsegueixin «tocar el llenç»."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:72
msgid "Connect Hairs"
msgstr "Connecta els cabells"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:73
msgid "The bristles get connected. See for yourself."
msgstr "Els pèls es connectaran. Vegeu-ho."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:74
msgid "Anti-Aliasing"
msgstr "Antialiàsing"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:75
msgid "This will decrease the jaggy-ness of the lines."
msgstr "Això farà disminuir els tremolors de les línies."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:77
msgid "Composite Bristles"
msgstr "Composició dels pèls"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:77
msgid ""
"This \"composes the bristle colors within one dab,\" but explains that the "
"effect is \"probably subtle\"."
msgstr ""
"Això «compon els colors del pèl dins d'un toc», però convé explicar que "
"l'efecte «probablement serà subtil»."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:80
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.2-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.2-2.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:82
msgid "Ink Depletion"
msgstr "Esgotament de la tinta"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:84
msgid ""
"This simulated ink depletion over drawing time. The value dictates how long "
"it will take. The curve dictates the speed."
msgstr ""
"Això simula l'esgotament de la tinta al llarg del temps de dibuix. El valor "
"dictarà durant quant de temps transcorrerà l'efecte. La corba dicta la "
"velocitat."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:86
msgid "Opacity"
msgstr "Opacitat"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:87
msgid "The brush will go transparent to simulate ink-depletion."
msgstr "El pinzell quedarà transparent per a simular l'esgotament de la tinta."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:89
msgid "The brush will be desaturated to simulate ink-depletion."
msgstr "El pinzell perdrà la saturació per a simular l'esgotament de la tinta."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:91
msgid "Saturation"
msgstr "Saturació"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:94
msgid ""
"The brush will pick up colors from other brushes. You don't need to have :"
"guilabel:`Ink depletion` checked to activate this option, you just have to "
"check :guilabel:`Soak ink`. What this does is cause the bristles of the "
"brush to take on the colors of the first area they touch. Since the Bristle "
"brush is made up of independent bristles, you can basically take on several "
"colors at the same time."
msgstr ""
"El pinzell recollirà els colors dels altres pinzells. No caldrà que marqueu :"
"guilabel:`Esgotament de la tinta` per activar aquesta opció, només haureu de "
"marcar :guilabel:`Suca tinta`. El que farà és fer que els pèls del pinzell "
"recullin els colors de la primera àrea que toquen. Atès que el pinzell de "
"pèl està format per pèls independents, bàsicament podrà prendre diversos "
"colors al mateix temps."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:98
msgid ""
"It will only take colors in the unscaled area of the brush, so if you're "
"using a brush with 4.00 scale for example, it will only take the colors in "
"the 1/4 area closest to the center."
msgstr ""
"Només prendrà colors en l'àrea sense escala del pinzell, de manera que si "
"empreu un pinzell amb una escala de 4,00, només prendrà els colors en l'àrea "
"d'1/4 més propera al centre."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:99
msgid "When the source is transparent, the bristles take black color."
msgstr "Quan la font és transparent, els pèls prendran el color negre."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:104
msgid "Soak Ink"
msgstr "Suca tinta"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:104
msgid ""
"Be aware that this feature is a bit buggy though. It's supposed to take the "
"color from the current layer, but some buggy behavior causes it to often use "
"the last layer you've painted on (with a non-Bristle brush?) as source. To "
"avoid these weird behaviors, stick to just one layer, or paint something on "
"the current active layer first with another brush (such as a Pixel brush)."
msgstr ""
"Recordeu que aquesta característica és una mica errònia. Se suposa que pren "
"el color de la capa actual, però alguns comportaments defectuosos fan que "
"sovint empri l'última capa que heu pintat (amb un pinzell sense pèl?) com a "
"font. Per evitar aquests comportaments estranys, ajusteu-vos a una sola "
"capa, o pinteu primer quelcom a l'actual capa activa amb un altre pinzell "
"(com un pinzell de píxels)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:107
msgid "Works by modifying the saturation with the following:"
msgstr "Treballa modificant la saturació amb el següent:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:109
msgid "Pressure weight"
msgstr "Pes de la pressió"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:110
msgid "Bristle length weight"
msgstr "Estil de la longitud del pèl"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:111
msgid "Bristle ink amount weight"
msgstr "Estil de la quantitat de tinta al pèl"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:112
msgid "Ink depletion curve weight"
msgstr "Estil de la corba per a l'esgotament de la tinta"
