# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-06-01 19:25+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_userManual.jpg"
msgstr ".. image:: images/intro_page/Hero_userManual.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_tutorials.jpg"
msgstr ".. image:: images/intro_page/Hero_tutorials.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_getting_started.jpg"
msgstr ".. image:: images/intro_page/Hero_getting_started.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_reference.jpg"
msgstr ".. image:: images/intro_page/Hero_reference.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_general.jpg"
msgstr ".. image:: images/intro_page/Hero_general.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_faq.jpg"
msgstr ".. image:: images/intro_page/Hero_faq.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_resources.jpg"
msgstr ".. image:: images/intro_page/Hero_resources.jpg"

#: ../../index.rst:5
msgid "Welcome to the Krita |version| Manual!"
msgstr "Välkommen till handboken för Krita |version|!"

#: ../../index.rst:7
msgid "Welcome to Krita's documentation page."
msgstr "Välkommen till Kritas dokumentationssida."

#: ../../index.rst:9
msgid ""
"Krita is a sketching and painting program designed for digital artists. Our "
"vision for Development of Krita is —"
msgstr ""
"Krita är ett tecknings- och målningsprogram konstruerat för "
"digitalkonstnärer. Vår vision för Kritas utveckling är —"

#: ../../index.rst:11
msgid ""
"Krita is a free and open source cross-platform application that offers an "
"end-to-end solution for creating digital art files from scratch. Krita is "
"optimized for frequent, prolonged and focused use. Explicitly supported "
"fields of painting are illustrations, concept art, matte painting, textures, "
"comics and animations. Developed together with users, Krita is an "
"application that supports their actual needs and workflow. Krita supports "
"open standards and interoperates with other applications."
msgstr ""
"Krita är en fritt program med öppen källkod för flera plattformar som "
"erbjuder en heltäckande lösning för att skapa digitalkonstfiler från början. "
"Krita är optimerat för upprepad, långvarig och fokuserad användning. "
"Målningstyper som explicit stöds är illustrationer, konceptkonst, matte "
"painting, strukturer, serier och animeringar. Krita är utvecklat tillsammans "
"med användare, som stödjer deras verkliga behov och arbetsflöde. Krita "
"stöder öppna standarder och arbetar tillsammans med andra program."

#: ../../index.rst:19
msgid ""
"Krita's tools are developed keeping the above vision in mind. Although it "
"has features that overlap with other raster editors its intended purpose is "
"to provide robust tool for digital painting and creating artworks from "
"scratch. As you learn about Krita, keep in mind that it is not intended as a "
"replacement for Photoshop. This means that the other programs may have more "
"features than Krita for image manipulation tasks, such as stitching together "
"photos, while Krita's tools are most relevant to digital painting, concept "
"art, illustration, and texturing. This fact accounts for a great deal of "
"Krita's design."
msgstr ""
"Kritas verktyg utvecklare med ovanstående vision i åtanke. Även om det har "
"funktioner som överlappar med andra rasteringseditorer, är dess avsedda "
"syfte att tillhandahålla ett robust verktyg för digital målning och för att "
"skapa konstverk från början. Medan du lär dig om Krita, kom ihåg att det "
"inte är avsett som en ersättare av Photoshop. Det betyder att andra program "
"kan ha flera funktioner än Krita för bildbehandlingsuppgifter, såsom att sy "
"ihop foton, medan Kritas verktyg är mest relevanta för digital målning, "
"konceptkonst, illustrationer och att skapa struktur. Detta faktum utgör "
"bakgrunden till en stor del av Kritas konstruktion."

#: ../../index.rst:28
msgid ""
"You can download this manual as an epub `here <https://docs.krita.org/en/"
"epub/KritaManual.epub>`_."
msgstr ""
"Man kan ladda ner handboken som en epub `här <https://docs.krita.org/en/epub/"
"KritaManual.epub>`_."

#: ../../index.rst:33
msgid ":ref:`user_manual`"
msgstr ":ref:`user_manual`"

#: ../../index.rst:33
msgid ":ref:`tutorials`"
msgstr ":ref:`tutorials`"

#: ../../index.rst:35
msgid ""
"Discover Krita’s features through an online manual. Guides to help you "
"transition from other applications."
msgstr ""
"Upptäck funktionerna i Krita med en nätbaserad handbok. Handledningar för "
"att hjälpa dig vid övergång från andra program."

#: ../../index.rst:35
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""
"Lär dig via handledningar skapade av utvecklare och användare som visar "
"Krita under användning."

#: ../../index.rst:41
msgid ":ref:`getting_started`"
msgstr ":ref:`getting_started`"

#: ../../index.rst:41
msgid ":ref:`reference_manual`"
msgstr ":ref:`reference_manual`"

#: ../../index.rst:43
msgid "New to Krita and don't know where to start?"
msgstr "Är Krita nytt för dig och vet du inte var du ska börja?"

#: ../../index.rst:43
msgid "A quick run-down of all of the tools that are available"
msgstr "En snabb genomgång av alla tillgängliga verktyg"

#: ../../index.rst:48
msgid ":ref:`general_concepts`"
msgstr ":ref:`general_concepts`"

#: ../../index.rst:48
msgid ":ref:`faq`"
msgstr ":ref:`faq`"

#: ../../index.rst:50
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr ""
"Lär dig om allmänna konst- och teknologibegrepp som inte är specifika för "
"Krita."

#: ../../index.rst:50
msgid ""
"Find answers to the most common questions about Krita and what it offers."
msgstr ""
"Sök efter svar på de vanligaste frågorna om Krita och vad det erbjuder."

#: ../../index.rst:55
msgid ":ref:`resources_page`"
msgstr ":ref:`resources_page`"

#: ../../index.rst:55
msgid ":ref:`genindex`"
msgstr ":ref:`genindex`"

#: ../../index.rst:57
msgid ""
"Textures, brush packs, and python plugins to help add variety to your "
"artwork."
msgstr ""
"Strukturer, penselpackar och Python-insticksprogram för att hjälpa till att "
"ge konstverk variation."

#: ../../index.rst:57
msgid "An index of the manual for searching terms by browsing."
msgstr "Ett index för handboken att bläddra igenom och söka efter begrepp."
