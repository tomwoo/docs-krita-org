# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-29 21:29+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:18
msgid ""
".. image:: images/icons/gradient_edit_tool.svg\n"
"   :alt: toolgradientedit"
msgstr ""
".. image:: images/icons/gradient_edit_tool.svg\n"
"   :alt: toolgradientedit"

#: ../../reference_manual/tools/gradient_edit.rst:1
msgid "Krita's vector gradient editing tool reference."
msgstr "Referens för Kritas toningsredigeringsverktyg."

#: ../../reference_manual/tools/gradient_edit.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/gradient_edit.rst:11
msgid "Gradient"
msgstr "Toning"

#: ../../reference_manual/tools/gradient_edit.rst:16
msgid "Gradient Editing Tool"
msgstr "Toningsredigeringsverktyg"

#: ../../reference_manual/tools/gradient_edit.rst:18
msgid "|toolgradientedit|"
msgstr "|toolgradientedit|"

#: ../../reference_manual/tools/gradient_edit.rst:22
msgid ""
"This tool has been removed in Krita 4.0, and its functionality has been "
"folded into the :ref:`shape_selection_tool`."
msgstr ""
"Det här verktyget har tagits bort i Krita 4.0, och dess funktionalitet har "
"integrerats i :ref:`shape_selection_tool`."

#: ../../reference_manual/tools/gradient_edit.rst:24
msgid ""
"This tool allows you to edit the gradient on canvas, but it only works for "
"vector layers. If you have a vector shape selected, and draw a line over the "
"canvas, you will be able to see the nodes, and the stops in the gradient. "
"Move around the nodes to move the gradient itself. Select the stops to "
"change their color in the tool options docker, or to move their position in "
"the on canvas gradient. You can select preset gradient in the tool docker to "
"change the active shape's gradient to use those stops."
msgstr ""
"Det här verktyget låter dig redigera toningar på duken, men det fungerar "
"bara på vektorlager. Om en vektorform är markerad, och en linje ritas på "
"duken, syns toningens noder och ändar. Flytta runt noderna för att flytta "
"själva toningen. Markera ändarna för att ändra deras färg i "
"verktygsalternativpanelen, eller för att flytta deras position på dukens "
"toning. Det går att välja förinställda toningar i verktygspanelen för att "
"ändra den aktiva formens toning att använda dessa ändar."
