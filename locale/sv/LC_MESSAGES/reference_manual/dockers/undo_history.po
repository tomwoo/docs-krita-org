# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:08+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Split strokes."
msgstr "Dela drag."

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../reference_manual/dockers/undo_history.rst:1
msgid "Overview of the undo history docker."
msgstr "Översikt av panelen för ångringshistorik."

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "Undo"
msgstr "Ångra"

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "Redo"
msgstr "Gör om"

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "History"
msgstr "Historik"

#: ../../reference_manual/dockers/undo_history.rst:16
msgid "Undo History"
msgstr "Ångringshistorik"

#: ../../reference_manual/dockers/undo_history.rst:19
msgid ".. image:: images/dockers/Krita_Undo_History_Docker.png"
msgstr ".. image:: images/dockers/Krita_Undo_History_Docker.png"

#: ../../reference_manual/dockers/undo_history.rst:20
msgid ""
"This docker allows you to quickly shift between undo states, and even go "
"back in time far more quickly that rapidly reusing the :kbd:`Ctrl + Z` "
"shortcut."
msgstr ""
"Panelen låter dig snabbt byta mellan ångringstillstånd, och till och med gå "
"tillbaka i tiden mycket fortare än att snabbt upprepa genvägen :kbd:`Ctrl + "
"Z`."

#: ../../reference_manual/dockers/undo_history.rst:22
msgid "Cumulate Undo"
msgstr "Ackumulerad ångring"

#: ../../reference_manual/dockers/undo_history.rst:25
msgid "Cumulative Undo"
msgstr "Ackumulerad ångring"

#: ../../reference_manual/dockers/undo_history.rst:27
msgid ""
"|mouseright| an item in the undo-history docker to enable cumulative undo. |"
"mouseright| again to change the parameters:"
msgstr ""
"Högerklicka på ett objekt i panelen ångringshistorik för att aktivera "
"ackumulerad ångring. Högerklicka igen för att ändra parametrarna:"

#: ../../reference_manual/dockers/undo_history.rst:29
msgid "Start merging time"
msgstr "Sammanslagningsstarttid"

#: ../../reference_manual/dockers/undo_history.rst:30
msgid ""
"The amount of seconds required to consider a group of strokes to be worth "
"one undo step."
msgstr ""
"Antal sekunder som krävs för att anse en grupp av drag vara värd ett "
"ångringssteg."

#: ../../reference_manual/dockers/undo_history.rst:31
msgid "Group time"
msgstr "Grupptid"

#: ../../reference_manual/dockers/undo_history.rst:32
msgid ""
"According to this parameter -- groups are made. Every stroke is put into the "
"same group till two consecutive strokes have a time gap of more than T "
"seconds. Then a new group is started."
msgstr ""
"Enligt den här parametern skapas grupper. Varje drag läggs till i samma "
"grupp tills två på varandra följande drag har ett tidsintervall som är mer "
"än T sekunder. Då påbörjas en ny grupp."

#: ../../reference_manual/dockers/undo_history.rst:34
msgid ""
"A user may want to keep the ability of Undoing/Redoing his last N strokes. "
"Once N is crossed -- the earlier strokes are merged into the group's first "
"stroke."
msgstr ""
"En användare kan vilja behålla möjligheten att ångra eller göra om de "
"senaste N dragen. När N väl har korsats sammanfogas de tidigare dragen i "
"gruppens första drag."

#~ msgid "..index:: Cumulate Undo"
#~ msgstr "..index:: Ackumulera ångring"
