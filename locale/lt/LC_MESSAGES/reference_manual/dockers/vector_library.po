# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 03:36+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../reference_manual/dockers/vector_library.rst:1
msgid "Overview of the vector library docker."
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:10
#: ../../reference_manual/dockers/vector_library.rst:15
msgid "Vector Library"
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:10
msgid "SVG Symbols"
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:10
msgid "Reusable Vector Shapes"
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:19
msgid ""
"The Vector Library Docker loads the symbol libraries in SVG files, when "
"those SVG files are put into the \"symbols\" folder in the resource folder :"
"menuselection:`Settings --> Manage Resources --> Open Resource Folder`."
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:21
msgid ""
"The vector symbols can then be dragged and dropped onto the canvas, allowing "
"you to quickly use complicated images."
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:23
msgid ""
"Currently, you cannot make symbol libraries with Krita yet, but you can make "
"them by hand, as well as use Inkscape to make them. Thankfully, there's "
"quite a few svg symbol libraries out there already!"
msgstr ""
