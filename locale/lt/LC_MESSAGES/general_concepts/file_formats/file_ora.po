# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-20 03:39+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../general_concepts/file_formats/file_ora.rst:1
msgid "The Open Raster Archive file format as exported by Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "*.ora"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "ORA"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "Open Raster Archive"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:15
msgid "\\*.ora"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:17
msgid ""
"``.ora``, or the Open Raster format, is an interchange format. It was "
"designed to replace :ref:`file_psd` as an interchange format, as the latter "
"isn't meant for that. Like :ref:`file_kra` it is loosely based on the Open "
"Document structure, thus a ZIP file with a bunch of XMLs and PNGs, but where "
"Krita's internal file format can sometimes have fully binary chunks, ``."
"ora`` saves its layers as :ref:`file_png` making it fully open and easy to "
"support."
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:19
msgid ""
"As an interchange format, it can be expected to be heavy and isn't meant for "
"uploading to the internet."
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:23
msgid "`Open Raster Specification <https://www.openraster.org/>`_"
msgstr ""
