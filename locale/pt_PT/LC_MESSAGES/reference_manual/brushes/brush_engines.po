# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-26 01:45+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/brushes/brush_engines.rst:5
msgid "Brush Engines"
msgstr "Motores de Pincéis"

#: ../../reference_manual/brushes/brush_engines.rst:7
msgid ""
"Information on the brush engines that can be accessed in the brush editor."
msgstr ""
"Informações sobre os motores de pincéis que podem ser acedidos no editor de "
"pincéis."

#: ../../reference_manual/brushes/brush_engines.rst:9
msgid "Available Engines:"
msgstr "Motores Disponíveis:"
