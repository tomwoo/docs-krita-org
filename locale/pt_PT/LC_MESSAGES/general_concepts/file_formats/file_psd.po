# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:51+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Photoshop filepdf Krita filesvg filetif fileora psd\n"
"X-POFile-SpellExtra: fileexr ref\n"

#: ../../general_concepts/file_formats/file_psd.rst:1
msgid "The Photoshop file format as exported by Krita."
msgstr "O formato de ficheiros do Photoshop, tal como é exportado pelo Krita."

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "*.psd"
msgstr "*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "PSD"
msgstr "PSD"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "Photoshop Document"
msgstr "Documento do Photoshop"

#: ../../general_concepts/file_formats/file_psd.rst:15
msgid "\\*.psd"
msgstr "\\*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:17
msgid ""
"``.psd`` is Photoshop's internal file format. For some reason, people like "
"to use it as an interchange format, even though it is not designed for this."
msgstr ""
"O ``.psd`` é o formato de ficheiros interno do Photoshop. Por alguma razão, "
"as pessoas gostam de o usar como um formato de intercâmbio, mesmo quando não "
"foi desenhado para tal."

#: ../../general_concepts/file_formats/file_psd.rst:19
msgid ""
"``.psd``, unlike actual interchange formats like :ref:`file_pdf`, :ref:"
"`file_tif`, :ref:`file_exr`, :ref:`file_ora` and :ref:`file_svg` doesn't "
"have an official spec online. Which means that it needs to be reverse "
"engineered. Furthermore, as an internal file format, it doesn't have much of "
"a philosophy to its structure, as it's only purpose is to save what "
"Photoshop is busy with, or rather, what all the past versions of Photoshop "
"have been busy with. This means that the inside of a PSD looks somewhat like "
"Photoshop's virtual brains, and PSD is in general a very disliked file-"
"format."
msgstr ""
"O ``.psd``, ao contrário dos formatos de intercâmbio, como o :ref:"
"`file_pdf`, :ref:`file_tif`, :ref:`file_exr`, :ref:`file_ora` e :ref:"
"`file_svg` não tem uma documentação de especificação oficial 'online'. O que "
"significa que é necessária alguma engenharia inversa. Para além disso, como "
"um formato de ficheiros interno, não tem propriamente uma filosofia face à "
"sua estrutura, já que o seu único objectivo é gravar o que o Photoshop "
"precisa ou, por outro lado, o que todas as outras versões mais antigas do "
"Photoshop precisam. Isto significa que o interior de um ficheiro PSD parece-"
"se algo semelhante ao cérebro virtual do Photoshop, e o PSD é um formato "
"pouco agradável, de um modo geral."

#: ../../general_concepts/file_formats/file_psd.rst:21
msgid ""
"Due to ``.psd`` being used as an interchange format, this leads to confusion "
"amongst people using these programs, as to why not all programs support "
"opening these. Sometimes, you might even see users saying that a certain "
"program is terrible because it doesn't support opening PSDs properly. But as "
"PSD is an internal file-format without online specs, it is impossible to "
"have any program outside it support it 100%."
msgstr ""
"Devido ao facto de o ``.psd`` ser usado como um formato de intercâmbio, isto "
"leva a alguma confusão entre as pessoas que usam estes programas, já que nem "
"todos os programas suportam a abertura deles. Em alguns casos, poderá até "
"ver utilizadores a dizer que um dado programa é terrível por não suportar "
"correctamente a abertura de PSD's. Porém, como o formato PSD é um formato de "
"ficheiros interno sem documentação 'online', é impossível ter qualquer "
"programa fora dele que o suporte a 100%."

#: ../../general_concepts/file_formats/file_psd.rst:23
msgid ""
"Krita supports loading and saving raster layers, blending modes, "
"layerstyles, layer groups, and transparency masks from PSD. It will likely "
"never support vector and text layers, as these are just too difficult to "
"program properly."
msgstr ""
"O Krita suporta o carregamento e a gravação de camadas rasterizadas, modos "
"de mistura, estilos de camadas, grupos de camadas e máscaras de "
"transparência do PSD. Provavelmente nunca irá suportar camadas vectoriais e "
"de texto, dado serem demasiado complicadas de programar correctamente."

#: ../../general_concepts/file_formats/file_psd.rst:25
msgid ""
"We recommend using any other file format instead of PSD if possible, with a "
"strong preference towards :ref:`file_ora` or :ref:`file_tif`."
msgstr ""
"Recomendamos que use qualquer outro formato de ficheiros em vez do PSD, se "
"possível, com uma forte preferência pelo :ref:`file_ora` ou :ref:`file_tif`."

#: ../../general_concepts/file_formats/file_psd.rst:27
msgid ""
"As a working file format, PSDs can be expected to become very heavy and most "
"websites won't accept them."
msgstr ""
"Como formato de ficheiro de trabalho, os PSD's podem-se tornar bastante "
"pesados e a maioria das páginas Web não têm suporte para eles."
